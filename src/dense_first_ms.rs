// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple standard-cell legalization (detail placement) with support for pre-placed macro blocks.
//! This is intended to be used for legalizing a large number of standard-cells
//! together with *pre-placed* and already legalized macro blocks (MS = mixed size).
//!
//! Yields decent results even if the cells are highly overlapping.

// libreda-db
use db::traits::*;
use db::{L2NBase, NetlistBase, SInt};
use libreda_pnr::db;

use libreda_pnr::libreda_db::iron_shapes::polygon::ToPolygon;

// pnr
use libreda_pnr::legalize::stdcell_legalizer::SimpleStdCellLegalizer;
use libreda_pnr::metrics::placement_density::DensityMap;

// workspace
use libreda_pnr::libreda_db::iron_shapes_booleanop::BooleanOp;

// crates.io
use itertools::Itertools;
use log::{debug, error, info, warn};

// stdlib
use iron_shapes_algorithms::rectangle_decomposition::decompose_rectangles;
use libreda_pnr::design::SimpleDesignRef;
use libreda_pnr::place::mixed_size_placer::{MixedSizePlacer, PlacementError};
use libreda_pnr::place::placement_problem::{PlacementProblem, PlacementStatus};
use std::collections::{HashMap, HashSet};

/// Simple standard-cell legalization (detail placement) with support for pre-placed macro blocks.
/// This is intended to be used for legalizing a large number of standard-cells
/// together with *pre-placed* and already legalized macro blocks (MS = mixed size).
///
/// Roughly, the algorithm places cells into the closest possible legal positions by
/// following a simple ordering. Cells in regions with higher densities are placed first.
pub struct DenseFirstLegalizerMS {
    /// Height of the standard-cell rows.
    row_height: db::Coord,
    /// Grid in x-direction for placing cells.
    grid_x: db::UInt,
}

impl DenseFirstLegalizerMS {
    /// Create a new legalizer instance.
    ///
    /// # Parameters
    /// * `row_height`: Height of the standard-cell rows.
    /// * `grid_x`: Place the cells at positions where the x-coordinate is an integer multiple of `grid_x`.
    pub fn new(row_height: db::Coord, grid_x: db::UInt) -> DenseFirstLegalizerMS {
        assert_ne!(grid_x, 0);
        assert_ne!(row_height, 0);
        DenseFirstLegalizerMS { row_height, grid_x }
    }
}

impl DenseFirstLegalizerMS {
    /// Find the best x-coordinate to place a cell within an interval.
    fn find_best_cell_position_x_in_interval(
        &self,
        pos_x: SInt,
        cell_width: SInt,
        limit: (SInt, SInt),
    ) -> Option<SInt> {
        let pitch = self.grid_x as SInt;

        fn grid_floor(x: SInt, pitch: SInt) -> SInt {
            x - (x % pitch)
        }
        fn grid_ceil(x: SInt, pitch: SInt) -> SInt {
            grid_floor(x + pitch - 1, pitch)
        }
        fn grid_round(x: SInt, pitch: SInt) -> SInt {
            grid_floor(x + pitch / 2, pitch)
        }

        if (limit.1 - limit.0) >= cell_width {
            let limit_left = grid_ceil(limit.0, pitch);
            let limit_right = grid_floor(limit.1 - cell_width, pitch);

            if limit_left <= limit_right {
                // Snap position to closest grid location within the limits.
                let pos_snapped = if pos_x <= limit_left {
                    limit_left
                } else if pos_x >= limit_right {
                    limit_right
                } else {
                    grid_round(pos_x, pitch)
                };
                debug_assert!(limit_left <= pos_snapped && pos_snapped <= limit_right);
                Some(pos_snapped)
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Find the a free position in the row that is closest to `x`.
    /// Return the index of the interval and the found x position if there is any.
    fn find_best_cell_position_x_in_row(
        &self,
        row: &Vec<(SInt, SInt)>,
        x: SInt,
        cell_width: SInt,
    ) -> Option<(usize, SInt)> {
        row.iter()
            // Add indices.
            .enumerate()
            // Find the best position in each interval.
            .filter_map(
                |(idx, &interval)| {
                    self.find_best_cell_position_x_in_interval(x, cell_width, interval)
                        .map(|pos| (idx, pos))
                }, // Attach index of interval.
            )
            // Find the closest position among all intervals.
            .min_by_key(|(_, pos)| (pos - x).abs())
    }

    /// Remove the space used by the cell from the row.
    fn insert_cell_into_row(
        &self,
        row: &mut Vec<(SInt, SInt)>,
        interval_idx: usize,
        pos_x: SInt,
        cell_width: SInt,
    ) {
        let interval = row[interval_idx];
        debug_assert!(interval.0 <= pos_x);
        debug_assert!(pos_x + cell_width <= interval.1);

        if interval.0 < pos_x && pos_x + cell_width < interval.1 {
            // Interval needs to be split in two intervals.
            let lower = (interval.0, pos_x);
            let upper = (pos_x + cell_width, interval.1);
            row[interval_idx] = lower; // Replace the old interval.
            row.insert(interval_idx + 1, upper); // Insert the new interval.
        } else if interval.0 == pos_x && interval.1 == pos_x + cell_width {
            // Cell spans the full interval.
            // Remove the interval.
            row.remove(interval_idx);
        } else if interval.0 == pos_x {
            // Cell is positioned at the start of the interval.
            // Shorten the interval.
            row[interval_idx] = (pos_x + cell_width, interval.1);
        } else if interval.1 == pos_x + cell_width {
            // Cell is positioned at the end of the interval.
            // Shorten the interval.
            row[interval_idx] = (interval.0, pos_x);
        }
    }

    /// Find the free position that is closest to the global position of the cell.
    /// Return a tuple (best free position, (row index, interval index)).
    fn find_best_cell_position(
        &self,
        rows: &Vec<Vec<(SInt, SInt)>>,
        row_y_coord: &Vec<db::SInt>,
        pos: db::Point<SInt>,
        cell_width: SInt,
    ) -> Option<(db::Point<SInt>, (usize, usize))> {
        // TODO: Optimize. There's no need to search all the rows.
        // TODO: Start width the closest row then expand from there in both y directions.
        let (closest_row_idx, _) = row_y_coord
            .iter()
            .enumerate()
            .min_by_key(|(_idx, y)| (**y - pos.y).abs())
            .expect("No row found");
        // TODO: Stop search as soon as the distance from the current row to the global position is larger that the best found position.

        let rows_up = (closest_row_idx..rows.len())
            .into_iter()
            .map(|i| (i, &rows[i]));
        let rows_down = (0..closest_row_idx)
            .rev()
            .into_iter()
            .map(|i| (i, &rows[i]));

        let interleaved_rows = rows_up.interleave(rows_down);

        let mut best = None;

        // Search for positions in rows.
        // Start with the closest row.
        for (i, row) in interleaved_rows {
            let row_idx = self
                .find_best_cell_position_x_in_row(row, pos.x, cell_width)
                .map(|p| (i, p));

            if let Some((i, (j, x))) = row_idx {
                let y = row_y_coord[i];
                let dy = (y - pos.y).abs() as u64;
                let dx = (x - pos.x).abs() as u64;
                let distance_sq = (dy * dy) + (dx * dx);

                if let Some((_, _, best_dist_sq)) = best {
                    if distance_sq < best_dist_sq {
                        best = Some((i, (j, x), distance_sq))
                    }

                    let row_dist_sq = dy * dy;
                    if row_dist_sq > best_dist_sq {
                        // Cannot find a closer location.
                        break;
                    }
                } else {
                    best = Some((i, (j, x), distance_sq))
                }
            }
        }

        best.map(|(i, (j, x), _d)| {
            let y = row_y_coord[i];
            (db::Point::new(x, y), (i, j))
        })
    }
}

impl<L: L2NBase<Coord = db::Coord>> MixedSizePlacer<L> for DenseFirstLegalizerMS {
    fn name(&self) -> &str {
        "DensityFirstMS"
    }

    fn find_cell_positions_impl(
        &self,
        placement_problem: &dyn PlacementProblem<L>,
    ) -> Result<HashMap<L::CellInstId, db::SimpleTransform<L::Coord>>, PlacementError> {
        info!("Start legalizer.");
        debug!("Row height: {}", self.row_height);

        let chip = placement_problem.fused_layout_netlist();
        let top_cell = placement_problem.top_cell();
        let top_ref = chip.cell_ref(&placement_problem.top_cell());

        let placement_sites = placement_problem.placement_region();

        let initial_positions: HashMap<_, _> = top_ref
            .each_cell_instance()
            .map(|inst| (inst.id(), placement_problem.initial_position(&inst.id())))
            .collect();

        // Convert placement sites to non-rectilinear polygons for boolean operations.
        let core_area = {
            let placement_sites: Vec<_> = placement_sites
                .iter()
                .map(|p| p.to_simple_polygon().into())
                .collect();
            db::MultiPolygon::from_polygons(placement_sites)
        };

        let core_area_bbox = core_area
            .try_bounding_box()
            .expect("Placement site has no defined bounding box.");

        debug!(
            "Core area bounding box size: {} * {}",
            core_area_bbox.width(),
            core_area_bbox.height()
        );

        let row_height = self.row_height;

        let grid_x = self.grid_x;
        debug!("Grid size in x-direction: {}", grid_x);

        // Get set of movable instances.
        let movable_instances: HashSet<_> = top_ref
            .each_cell_instance()
            .filter(|inst| {
                placement_problem.placement_status(&inst.id()) == PlacementStatus::Movable
            })
            .map(|inst| inst.id())
            .collect();
        let fixed_instances: HashSet<_> = top_ref
            .each_cell_instance()
            .filter(|inst| placement_problem.placement_status(&inst.id()) == PlacementStatus::Fixed)
            .map(|inst| inst.id())
            .collect();

        // Test that all movable cells have the right height.
        // TODO: Do this in libreda-pnr.
        let with_wrong_height: HashSet<_> = {
            // Find cell types of the movable instances.
            let movable_cell_types: HashSet<L::CellId> = movable_instances
                .iter()
                .map(|inst| chip.template_cell(inst))
                .collect();
            // Find the cell types with a height != row height.
            movable_cell_types
                .into_iter()
                .filter(|c| {
                    placement_problem
                        .cell_outline(c)
                        .map(|r| r.height() != row_height)
                        .unwrap_or(true)
                })
                .collect()
        };
        if !with_wrong_height.is_empty() {
            warn!(
                "Number of cells with wrong height: {}",
                with_wrong_height.len()
            );
            let names = with_wrong_height
                .iter()
                .map(|c| chip.cell_name(c))
                .sorted()
                .join(", ");
            warn!(
                "Cells with wrong height will be excluded from legalization: {}",
                names
            );
        }

        // Treat cells with wrong height (Macro cells) as non-movable.
        let instances_with_wrong_height: HashSet<_> = {
            movable_instances
                .iter()
                .filter(|inst| {
                    let cell_id = chip.template_cell(inst);
                    with_wrong_height.contains(&cell_id)
                })
                .cloned()
                .collect()
        };
        let movable_instances: HashSet<_> = movable_instances
            .difference(&instances_with_wrong_height)
            .cloned()
            .collect();
        let fixed_instances: HashSet<_> = fixed_instances
            .union(&instances_with_wrong_height)
            .cloned()
            .collect();

        let num_rows = (core_area_bbox.height() / row_height as db::SInt) as usize;
        debug!("Number of std-cell rows: {}", num_rows);

        let row_width = core_area_bbox.width();
        debug!("Row width: {}", row_width);

        // Get y-coordinates of the rows.
        let row_y_coord: Vec<db::Coord> = (0..num_rows)
            .map(|i| i as db::Coord * row_height as db::Coord + core_area_bbox.lower_left().y)
            .collect_vec();

        // Get list of all cell indices to be placed.
        let all_instances = {
            let mut c = movable_instances.iter().cloned().collect_vec();
            // Sort cells by x-coordinate.
            c.sort_by_key(|id| {
                db::Point::from(placement_problem.initial_position(id).displacement)
            });
            c
        };

        // Get all cell areas.
        let mut cells_without_outline = HashSet::new();
        let cell_boxes: HashMap<_, _> = chip
            .each_cell_instance(&top_cell)
            .flat_map(|inst| {
                let outline = placement_problem.cell_instance_outline(&inst);
                if outline.is_none() {
                    cells_without_outline.insert(chip.template_cell(&inst));
                }
                outline.map(|outline| (inst, outline))
            })
            .collect();

        for cell in cells_without_outline {
            log::warn!("Cell has no outline: {}", chip.cell_name(&cell));
        }

        // Size of a bin used to compute the average density.
        let bin_size = (self.row_height * 4) as db::Coord;
        let float_bbox: db::Rect<f64> = core_area_bbox.cast();
        let mut density_bins = DensityMap::new(
            float_bbox,
            (
                (core_area_bbox.width() / bin_size) as usize,
                (core_area_bbox.height() / bin_size) as usize,
            ),
        );

        {
            // Init density bins.
            // Set density to 0 where cells can be placed, 1 otherwise.
            let r = density_bins.dimension;
            density_bins.draw_rect(&r, 1.0);
            let rects = decompose_rectangles(placement_sites.iter().flat_map(|p| p.edges()));
            // Mark regions where cells can be put.
            for r in rects {
                density_bins.draw_rect(&r.cast(), -1.0);
            }
        }

        // Limit the initial positions of movable cells to the core area.
        let cell_positions: HashMap<_, _> = initial_positions
            .iter()
            .map(|(id, tf)| {
                if movable_instances.contains(id) {
                    let ll = core_area_bbox.lower_left();
                    let ur = core_area_bbox.upper_right();
                    let p = tf.transform_point(db::Point::zero());
                    let p_limit = db::Point::new(p.x.max(ll.x).min(ur.x), p.y.max(ll.y).min(ur.y));
                    let tf = db::SimpleTransform {
                        displacement: p_limit.into(),
                        ..*tf
                    };
                    (id.clone(), tf)
                } else {
                    (id.clone(), *tf)
                }
            })
            .collect();

        // Put cells into bins.
        // Accumulate the overlapping area with the bin to find the cell density.
        log::debug!("Create density map.");
        for inst in &all_instances {
            let tf = &cell_positions[inst];
            // Get the bounding box of the positioned cell.
            let cell_box = cell_boxes[inst].transform(|p| tf.transform_point(p));

            debug_assert!(cell_box.width() < bin_size);
            debug_assert!(cell_box.height() < bin_size);

            density_bins.draw_rect(&cell_box.cast(), 1.0);
        }

        // /// Remove all free space in the `occupied` interval.
        // fn mark_interval_as_occupied(row: &Vec<(SInt, SInt)>, occupied: (SInt, SInt)) -> Vec<(SInt, SInt)> {
        //     let mut output = Vec::new();
        //     output.reserve(row.len());
        //     for &interval in row {
        //         let b = interval.1.min(occupied.0);
        //         if interval.0 < b {
        //             // Non-empty interval. Store it.
        //             output.push((interval.0, b));
        //         }
        //
        //         let a = interval.0.max(occupied.1);
        //         if a < interval.1 {
        //             // Non-empty interval. Store it.
        //             output.push((a, interval.1));
        //         }
        //     }
        //     output
        // }

        // Create obstructions from fixed cells.
        let obstructions: Vec<_> = chip
            .each_cell_instance(&top_cell)
            .filter(|inst| fixed_instances.contains(&inst)) // Skip movable instances.
            .flat_map(|inst| {
                // ignore the cells that don't have an outline.
                let bbox = cell_boxes.get(&inst);

                // Move to actual location.
                bbox.map(|bbox| {
                    let pos = initial_positions[&inst];
                    bbox.transform(|p| pos.transform_point(p))
                })
            })
            .map(|mut bbox| {
                // Enlarge bbox to fully block touched rows.
                bbox.lower_left.y -= row_height;
                bbox.upper_right.y += row_height;
                bbox.to_polygon()
            })
            .collect();
        log::info!("Number of obstruction shapes: {}", obstructions.len());
        let obstructions = db::MultiPolygon::from_polygons(obstructions);
        let valid_placement_area: db::MultiPolygon<_> = core_area.difference(&obstructions);
        log::info!(
            "Number of placement area partitions: {}",
            valid_placement_area.len()
        );
        log::info!(
            "Number of holes in placement area: {}",
            valid_placement_area
                .polygons
                .iter()
                .map(|p| p.interiors.len())
                .sum::<usize>()
        );

        // Find number of standard-cell rows.
        let num_rows = (core_area_bbox.height() / self.row_height) as u32;
        // Store the unused space in each row as intervals of the form (start, end).
        // Initialize the intervals with the intersection of the row with the core area.
        let mut rows_free: Vec<_> = (0..num_rows)
            .map(|row_i| {
                let row_start_y = row_y_coord[row_i as usize];
                let full_row_shape = db::Rect::new(
                    (core_area_bbox.lower_left().x, row_start_y),
                    (core_area_bbox.upper_right().x, row_start_y + row_height),
                )
                .to_polygon();
                let full_row_shape = db::MultiPolygon::from_polygons(vec![full_row_shape]);
                let intersection = full_row_shape.intersection(&valid_placement_area);

                let valid_intervals: Vec<_> = intersection
                    .polygons
                    .iter()
                    .flat_map(|p| p.try_bounding_box())
                    .sorted_by_key(|r| r.lower_left().x)
                    .map(|r| (r.lower_left().x, r.upper_right().x))
                    .collect();

                assert!(
                    valid_intervals
                        .iter()
                        .zip(valid_intervals.iter().skip(1))
                        .all(|((_, a1), (b0, _))| a1 <= b0),
                    "Intervals should not overlap."
                );

                valid_intervals
            })
            .collect();

        // Sort the cells by descending density.
        let cells_by_density_descending = all_instances
            .iter()
            .sorted_by_key(|id| {
                let tf = &initial_positions[id];
                let center = tf.transform_point(cell_boxes[id].center());
                ((density_bins.get_density_at(center.cast()).unwrap_or(1.0)) * 1000.) as i32
                // Pseudo fix-point to implement Ord.
            })
            .rev()
            .collect_vec();

        // // TODO Reserve space for fixed cells.
        // {
        //     let start_row;
        //     let end_row;
        //     for i in start_row..end_row+1 {
        //         // Mark the position as used.
        //         insert_cell_into_row(&mut rows_free[i], j, p.x, cell_width);
        //     }
        // }

        // Associate circuit instances with transformations that describe their location and orientation.
        let mut transformations: HashMap<L::CellInstId, db::SimpleTransform<_>> = HashMap::new();

        // Place cells in high-density regions first.
        log::debug!("Place cells. Start in high-density regions.");
        for cell in cells_by_density_descending {
            let tf = cell_positions[cell];
            let cell_box = cell_boxes[cell];
            let cell_width = cell_box.width();
            let cell_height = cell_box.height();
            let pos = tf.transform_point(cell_box.center());
            let best_pos = self.find_best_cell_position(&rows_free, &row_y_coord, pos, cell_width);
            if let Some((p, (i, j))) = best_pos {
                // Mark the position as used.
                self.insert_cell_into_row(&mut rows_free[i], j, p.x, cell_width);

                // Make sure that cells face each other always nwell-to-nwell and pwell-to-pwell.
                // So cells in every second row must be rotated by 180 degrees (or alternatively mirrored).
                let flip = i % 2 == 1;
                let (rotation, x, y) = if flip {
                    (db::Angle::R180, p.x + cell_width, p.y + cell_height)
                } else {
                    (db::Angle::R0, p.x, p.y)
                };
                let mirror = false;

                let tf = db::SimpleTransform::new(mirror, rotation, 1, db::Vector::new(x, y));

                transformations.insert(cell.clone(), tf);
            } else {
                // return Err() // TODO: Return error: Err((legalized cells, non-legalized cells))
                error!("No free space: Failed to find legal positions for all cells.");
                panic!("No free space: Failed to find legal positions for all cells.");
            }
        }
        log::debug!("Done");

        Ok(transformations)
    }
}

impl<L: NetlistBase + L2NBase<Coord = db::Coord>> SimpleStdCellLegalizer<L>
    for DenseFirstLegalizerMS
{
    fn find_legal_positions_impl(
        &self,
        netlist: &L,
        top_cell: &L::CellId,
        core_area: &db::SimplePolygon<db::SInt>,
        cell_outlines: &HashMap<L::CellId, db::Rect<db::SInt>>,
        global_positions: &HashMap<L::CellInstId, db::Point<db::SInt>>,
        movable_instances: &HashSet<L::CellInstId>,
    ) -> HashMap<L::CellInstId, db::SimpleTransform<db::SInt>> {
        // Adapter function. Passes the call to `find_legal_positions`.

        let initial_positions = global_positions
            .iter()
            .map(|(id, pos)| (id.clone(), db::SimpleTransform::translate(pos.v())))
            .collect();

        let core_area = db::SimpleRPolygon::try_new(core_area.points())
            .expect("Core area must be a rectilinear polygon.");

        let placement_status = netlist
            .each_cell_instance(&top_cell)
            .map(|inst| {
                let status = if movable_instances.contains(&inst) {
                    PlacementStatus::Movable
                } else {
                    PlacementStatus::Fixed
                };

                (inst.clone(), status)
            })
            .collect();

        let result = (self as &dyn MixedSizePlacer<L>).find_cell_positions_impl(&SimpleDesignRef {
            fused_layout_netlist: netlist,
            top_cell: top_cell.clone(),
            cell_outlines: cell_outlines,
            placement_region: &vec![core_area],
            placement_status: &placement_status,
            net_weights: &Default::default(),
            placement_location: &initial_positions,
        });

        if result.is_err() {
            log::error!("Legalization failed.");
        }

        result.expect("Legalization failed.")
    }
}
