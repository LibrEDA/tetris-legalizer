// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple standard-cell legalization (detail placement).
//!
//! Yields decent results even if the cells are highly overlapping.

use db::{ToPolygon, TryBoundingBox, TryCastCoord};
use itertools::Itertools;
use libreda_pnr::db;
use libreda_pnr::db::{Coord, NetlistBase, SInt, Translate};
use libreda_pnr::legalize::stdcell_legalizer::SimpleStdCellLegalizer;
use log::{debug, error, info, warn};
use std::collections::{HashMap, HashSet};

/// Simple standard-cell legalizer implementation.
///
/// Roughly, the algorithm places cells into the closest possible legal positions by
/// following a simple ordering. Cells in regions with higher densities are placed first.
pub struct DenseFirstLegalizer {
    /// Height of the standard-cell rows.
    row_height: db::Coord,
    /// Grid in x-direction for placing cells.
    grid_x: db::UInt,
}

impl DenseFirstLegalizer {
    /// Create a new legalizer instance.
    ///
    /// # Parameters
    /// * `row_height`: Height of the standard-cell rows.
    /// * `grid_x`: Place the cells at positions where the x-coordinate is an integer multiple of `grid_x`.
    pub fn new(row_height: db::Coord, grid_x: db::UInt) -> DenseFirstLegalizer {
        assert_ne!(grid_x, 0);
        assert_ne!(row_height, 0);
        DenseFirstLegalizer { row_height, grid_x }
    }
}

impl<N: NetlistBase> SimpleStdCellLegalizer<N> for DenseFirstLegalizer {
    fn find_legal_positions_impl(
        &self,
        netlist: &N,
        circuit: &N::CellId,
        core_area: &db::SimplePolygon<db::SInt>,
        cell_outlines: &HashMap<N::CellId, db::Rect<db::SInt>>,
        global_positions: &HashMap<N::CellInstId, db::Point<db::SInt>>,
        movable_instances: &HashSet<N::CellInstId>,
    ) -> HashMap<N::CellInstId, db::SimpleTransform<db::SInt>> {
        info!("Start legalizer.");
        debug!("Row height: {}", self.row_height);

        // Check that the core area is a rectangle and convert it to a rectangle.
        let core_area_bbox = core_area.try_bounding_box().unwrap();
        let core_area_poly = core_area_bbox.to_polygon().exterior;
        if &core_area_poly != core_area {
            let msg = "Shapes other than rectangles are not yet supported as core area.";
            error!("{}", msg);
            panic!("{}", msg);
        }
        let core_area: db::Rect<_> = core_area_bbox;

        debug!(
            "Core are size: {} * {}",
            core_area.width(),
            core_area.height()
        );

        let row_height = self.row_height;

        let grid_x = self.grid_x;
        debug!("Grid size in x-direction: {}", grid_x);

        // Test that all cells have the right height.
        // TODO: Do this in libreda-pnr.
        let with_wrong_height: Vec<_> = netlist
            .each_dependent_cell(circuit)
            .filter(|c| {
                cell_outlines
                    .get(c)
                    .map(|r| r.height() != row_height)
                    .unwrap_or(true)
            })
            .collect();
        if !with_wrong_height.is_empty() {
            warn!(
                "Number of cells with wrong height: {}",
                with_wrong_height.len()
            );
            let names = with_wrong_height
                .iter()
                .map(|c| netlist.cell_name(c))
                .join(", ");
            warn!("Cells with wrong height: {}", names);
        }

        let num_rows = (core_area.height() / row_height as db::SInt) as usize;
        debug!("Number of std-cell rows: {}", num_rows);

        let row_width = core_area.width();
        debug!("Row width: {}", row_width);

        // Get y-coordinates of the rows.
        let row_y_coord: Vec<db::SInt> = (0..num_rows)
            .map(|i| i as db::SInt * row_height as db::SInt + core_area.lower_left().y)
            .collect_vec();

        // Get list of all cell indices to be placed.
        let all_instances = {
            let mut c = movable_instances.iter().cloned().collect_vec();
            // Sort cells by x-coordinate.
            c.sort_by_key(|id| global_positions[id]);
            c
        };

        // Get all cell areas.
        let cell_boxes: HashMap<_, _> = all_instances
            .iter()
            .map(|inst| {
                let cell_id = netlist.template_cell(inst);
                (inst.clone(), cell_outlines[&cell_id].clone())
            })
            .collect();

        // Size of a bin used to compute the average density.
        let bin_size = (self.row_height * 4) as SInt;
        // Convert coordinates of cells into bin indices.
        let bin_indices = |x: Coord, y: Coord| -> (SInt, SInt) {
            let i = (x - core_area_bbox.lower_left().x) / bin_size;
            let j = (y - core_area_bbox.lower_left().y) / bin_size;
            (i, j)
        };
        // Get the rectangular boundary of the bin by its index.
        let bin_shape = |i: SInt, j: SInt| -> db::Rect<SInt> {
            let p0 = core_area_bbox.lower_left() + db::Point::new(i, j).cast() * bin_size;
            let p1 = p0 + db::Point::new(bin_size, bin_size).cast();
            db::Rect::new(p0, p1)
        };

        // Cells put into the bin which intersects with their lower left corner.
        let mut bins: HashMap<(SInt, SInt), HashSet<N::CellInstId>> = HashMap::new();
        // Total area of cells overlapping with the bin.
        let mut bin_content: HashMap<(SInt, SInt), i64> = HashMap::new();

        // Limit the global positions to the core area.
        let cell_positions: HashMap<_, _> = global_positions
            .iter()
            .map(|(id, p)| {
                let ll = core_area_bbox.lower_left();
                let ur = core_area_bbox.upper_right();
                (
                    id.clone(),
                    db::Point::new(p.x.max(ll.x).min(ur.x), p.y.max(ll.y).min(ur.y)),
                )
            })
            .collect();

        // Put cells into bins.
        // Accumulate the overlapping area with the bin to find the cell density.
        for inst in &all_instances {
            let pos = cell_positions[inst];
            // Get the bounding box of the positioned cell.
            let cell_box = cell_boxes[inst].translate(pos.v());

            debug_assert!(cell_box.width() < bin_size);
            debug_assert!(cell_box.height() < bin_size);

            // Update all bins that overlap with the cell.
            // TODO: This method only works if the cell is smaller than the bin.
            for p in cell_box.to_polygon().exterior.iter() {
                let (i, j) = bin_indices(p.x, p.y);
                let bin_box = bin_shape(i, j);
                if let Some(overlap) = bin_box.intersection(&cell_box) {
                    // Store the cells that touch this bin.
                    bins.entry((i, j))
                        .or_insert(Default::default())
                        .insert(inst.clone());
                    // Accumulate the overlapping cell area.
                    *bin_content.entry((i, j)).or_insert(Default::default()) +=
                        (overlap.width() * overlap.height()) as i64;
                }
            }
        }

        // Find number of standard-cell rows.
        let num_rows = (core_area_bbox.height() / self.row_height) as u32;
        // Store the unused space in each row as intervals of the form (start, end).
        // Initialize the intervals with the full width of the core.
        let mut rows_free = (0..num_rows)
            .map(|_| {
                vec![(
                    core_area_bbox.lower_left().x,
                    core_area_bbox.upper_right().x,
                )]
            })
            .collect_vec();

        /// Find the best x-coordinate to place a cell within an interval.
        fn find_best_cell_position_x_in_interval(
            pos_x: SInt,
            cell_width: SInt,
            limit: (SInt, SInt),
        ) -> Option<SInt> {
            if (limit.1 - limit.0) >= cell_width {
                Some(pos_x.max(limit.0).min(limit.1 - cell_width))
            } else {
                None
            }
        }

        /// Find the a free position in the row that is closest to `x`.
        /// Return the index of the interval and the found x position if there is any.
        fn find_best_cell_position_x_in_row(
            row: &Vec<(SInt, SInt)>,
            x: SInt,
            cell_width: SInt,
        ) -> Option<(usize, SInt)> {
            row.iter()
                // Add indices.
                .enumerate()
                // Find the best position in each interval.
                .filter_map(
                    |(idx, &interval)| {
                        find_best_cell_position_x_in_interval(x, cell_width, interval)
                            .map(|pos| (idx, pos))
                    }, // Attach index of interval.
                )
                // Find the closest position among all intervals.
                .min_by_key(|(_, pos)| (pos - x).abs())
        }

        /// Remove the space used by the cell from the row.
        fn insert_cell_into_row(
            row: &mut Vec<(SInt, SInt)>,
            interval_idx: usize,
            pos_x: SInt,
            cell_width: SInt,
        ) {
            let interval = row[interval_idx];
            debug_assert!(interval.0 <= pos_x);
            debug_assert!(pos_x + cell_width <= interval.1);

            if interval.0 < pos_x && pos_x + cell_width < interval.1 {
                // Interval needs to be split in two intervals.
                let lower = (interval.0, pos_x);
                let upper = (pos_x + cell_width, interval.1);
                row[interval_idx] = lower; // Replace the old interval.
                row.insert(interval_idx + 1, upper); // Insert the new interval.
            } else if interval.0 == pos_x && interval.1 == pos_x + cell_width {
                // Cell spans the full interval.
                // Remove the interval.
                row.remove(interval_idx);
            } else if interval.0 == pos_x {
                // Cell is positioned at the start of the interval.
                // Shorten the interval.
                row[interval_idx] = (pos_x + cell_width, interval.1);
            } else if interval.1 == pos_x + cell_width {
                // Cell is positioned at the end of the interval.
                // Shorten the interval.
                row[interval_idx] = (interval.0, pos_x);
            }
        }

        /// Find the free position that is closest to the global position of the cell.
        /// Return a tuple (best free position, (row index, interval index)).
        fn find_best_cell_position(
            rows: &Vec<Vec<(SInt, SInt)>>,
            row_y_coord: &Vec<db::SInt>,
            pos: db::Point<SInt>,
            cell_width: SInt,
        ) -> Option<(db::Point<SInt>, (usize, usize))> {
            // TODO: Optimize. There's no need to search all the rows.
            // TODO: Start width the closest row then expand from there in both y directions.
            // TODO: Stop search as soon as the distance from the current row to the global position is larger that the best found position.
            rows.iter()
                .enumerate()
                .filter_map(|(i, row)| {
                    find_best_cell_position_x_in_row(row, pos.x, cell_width).map(|p| (i, p))
                })
                // Find the closest fit by euclidean distance.
                .min_by_key(|&(i, (_j, x))| {
                    let y = row_y_coord[i];
                    let dy = (y - pos.y).abs() as u64;
                    let dx = (x - pos.x).abs() as u64;
                    (dy * dy) + (dx * dx)
                })
                .map(|(i, (j, x))| {
                    let y = row_y_coord[i];

                    (db::Point::new(x, y), (i, j))
                })
        }

        // Sort bins by density.
        let bins_by_density_ascending = bins
            .keys()
            .copied()
            .sorted_by_key(|bin_idx| bin_content[bin_idx])
            .collect_vec();

        // For each cell find the density around it.
        let density_around_cell: HashMap<_, _> = bins_by_density_ascending
            .iter()
            .flat_map(|bin_idx| {
                let cells = &bins[bin_idx];
                let density = bin_content[bin_idx];
                cells.iter().map(move |c| (c.clone(), density))
            })
            .collect();

        // Sort the cells by descending density.
        let cells_by_density_descending = all_instances
            .iter()
            .sorted_by_key(|id| density_around_cell[id])
            .rev()
            .collect_vec();

        // Associate circuit instances with transformations that describe their location and orientation.
        let mut transformations: HashMap<N::CellInstId, db::SimpleTransform<_>> = HashMap::new();

        // Place cells in high-density regions first.
        for cell in cells_by_density_descending {
            let pos = cell_positions[cell];
            let cell_box = cell_boxes[cell];
            let cell_width = cell_box.width();
            let cell_height = cell_box.height();
            let best_pos = find_best_cell_position(&rows_free, &row_y_coord, pos, cell_width);
            if let Some((p, (i, j))) = best_pos {
                // Mark the position as used.
                insert_cell_into_row(&mut rows_free[i], j, p.x, cell_width);

                // Make sure that cells face each other always nwell-to-nwell and pwell-to-pwell.
                // So cells in every second row must be rotated by 180 degrees (or alternatively mirrored).
                let flip = i % 2 == 1;
                let (rotation, x, y) = if flip {
                    (db::Angle::R180, p.x + cell_width, p.y + cell_height)
                } else {
                    (db::Angle::R0, p.x, p.y)
                };
                let mirror = false;

                let tf = db::SimpleTransform::new(mirror, rotation, 1, db::Vector::new(x, y));

                transformations.insert(cell.clone(), tf);
            } else {
                // return Err() // TODO: Return error: Err((legalized cells, non-legalized cells))
                error!("No free space: Failed to find legal positions for all cells.");
                panic!("No free space: Failed to find legal positions for all cells.");
            }
        }

        transformations
    }
}
