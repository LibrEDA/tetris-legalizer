// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple standard-cell legalization (detail placement).
//!
//! This crate implements three standard-cell legalization algorithms:
//! * [`TetrisLegalizer`] - Simple and stupid legalization algorithm. Not suitable much for more than demonstration or education.
//! Less than 200 lines of code.
//! * [`DenseFirstLegalizer`] - Better legalization algorithm. Does not support pre-placed macros.
//! * [`DenseFirstLegalizerMS`] - Support for pre-placed macros and complicated placement-area shapes.
//!
//! A legalizer takes rough cell positions from the global placement step and converts them
//! into legal (non-overlapping) cell positions.
//!
//!

#![deny(missing_docs)]

mod dense_first;
mod dense_first_ms;
mod tetris;

pub use crate::dense_first::DenseFirstLegalizer;
pub use crate::dense_first_ms::DenseFirstLegalizerMS;
pub use crate::tetris::TetrisLegalizer;
