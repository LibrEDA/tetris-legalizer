// Copyright (c) 2020-2021 Thomas Kramer.
// SPDX-FileCopyrightText: 2022 Thomas Kramer <code@tkramer.ch>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

//! Simple-stupid standard-cell legalization (detail placement) based on playing tetris with the
//! standard cells.
//!
//! This is just an example. The algorithm does not perform well in practice.

pub use libreda_pnr::db::{ToPolygon, TryBoundingBox};

use itertools::Itertools;
use libreda_pnr::db;
use libreda_pnr::db::NetlistBase;
use libreda_pnr::legalize::stdcell_legalizer::SimpleStdCellLegalizer;
use log::{debug, error, info, warn};
use std::collections::{HashMap, HashSet};

/// Simple standard-cell legalizer implementation.
///
/// Roughly, the algorithm implemented here sorts cells by x-coordinates and then takes one cell after
/// the other and tries to fit them onto the closest row. Rows are filled from left to right.
pub struct TetrisLegalizer {
    /// Height of the standard-cell rows.
    row_height: db::Coord,
    /// Grid in x-direction for placing cells.
    grid_x: db::UInt,
}

impl TetrisLegalizer {
    /// Create a new Tetris legalizer instance.
    ///
    /// # Parameters
    /// * `row_height`: Height of the standard-cell rows.
    /// * `grid_x`: Place the cells at positions where the x-coordinate is an integer multiple of `grid_x`.
    pub fn new(row_height: db::Coord, grid_x: db::UInt) -> TetrisLegalizer {
        assert_ne!(grid_x, 0);
        assert_ne!(row_height, 0);
        TetrisLegalizer { row_height, grid_x }
    }
}

impl<N: NetlistBase> SimpleStdCellLegalizer<N> for TetrisLegalizer {
    fn find_legal_positions_impl(
        &self,
        netlist: &N,
        circuit: &N::CellId,
        core_area: &db::SimplePolygon<db::SInt>,
        cell_outlines: &HashMap<N::CellId, db::Rect<db::SInt>>,
        global_positions: &HashMap<N::CellInstId, db::Point<db::SInt>>,
        movable_instances: &HashSet<N::CellInstId>,
    ) -> HashMap<N::CellInstId, db::SimpleTransform<db::SInt>> {
        info!("Start Tetris legalizer.");
        debug!("Row height: {}", self.row_height);

        // Check that the core area is a rectangle and convert it to a rectangle.
        let core_area_bbox = core_area.try_bounding_box().unwrap();
        let core_area_poly = core_area_bbox.to_polygon().exterior;
        if &core_area_poly != core_area {
            let msg = "Shapes other than rectangles are not yet supported as core area.";
            error!("{}", msg);
            panic!("{}", msg);
        }
        let core_area: db::Rect<_> = core_area_bbox;

        debug!(
            "Core are size: {} * {}",
            core_area.width(),
            core_area.height()
        );

        let row_height = self.row_height;

        let grid_x = self.grid_x;
        debug!("Grid size in x-direction: {}", grid_x);

        // Test that all cells have the right height.
        let with_wrong_height: Vec<_> = netlist
            .each_dependent_cell(circuit)
            .filter(|c| {
                cell_outlines
                    .get(c)
                    .map(|b| b.height() != row_height)
                    .unwrap_or(true)
            })
            .collect();
        if !with_wrong_height.is_empty() {
            warn!(
                "Number of cells with wrong height: {}",
                with_wrong_height.len()
            )
        }

        let num_rows = (core_area.height() / row_height as db::SInt) as usize;
        debug!("Number of std-cell rows: {}", num_rows);

        let row_width = core_area.height();
        debug!("Row width: {}", row_width);

        // List for keeping track of the row filling status.
        let mut row_length = vec![0; num_rows];

        // Get y-coordinates of the rows.
        let row_y_coord: Vec<db::SInt> = row_length
            .iter()
            .enumerate()
            .map(|(i, _)| i as db::SInt * row_height as db::SInt + core_area.lower_left().y)
            .collect_vec();

        // Get list of all cell indices to be placed.
        let all_cells = {
            let mut c = movable_instances.iter().cloned().collect_vec();
            // Sort cells by x-coordinate.
            c.sort_by_key(|id| global_positions[id]);
            c
        };

        // Associate circuit instances with transformations that describe their location and orientation.
        let mut transformations: HashMap<N::CellInstId, db::SimpleTransform<_>> = HashMap::new();

        // Play Tetris and find positions and rotations for the cells.
        for cell_id in all_cells {
            // Get global position of the cell.
            let pos = global_positions
                .get(&cell_id)
                .expect("Global position is not defined for this circuit instance.");
            // Get the shape of this cell.
            let shape = *cell_outlines
                .get(&netlist.template_cell(&cell_id))
                .expect("Shape is not defined for this circuit instance.");
            let (w, h) = (shape.width(), shape.height());
            debug_assert_eq!(h, row_height);

            // Find the row and x position where to place this cell.

            let best_position = row_length
                .iter()
                .enumerate()
                // Find rows that have space left for this cell.
                .filter(|(_i, l)| *l + (w as db::SInt) < row_width)
                .map(|(i, l)| {
                    // Get best x-position for every row.
                    let x = pos.x.max(*l);
                    // Round up to next grid position.
                    let x_snapped =
                        ((x + grid_x as db::SInt - 1) / grid_x as db::SInt) * grid_x as db::SInt;

                    // Limit to the core boundary:
                    // Find most right position for the cell on this row.
                    let x_max = core_area.width() - w as db::SInt;
                    // Round down to next grid position.
                    let x_max_snapped = (x_max / grid_x as db::SInt) * grid_x as db::SInt;
                    let x_limited = x_snapped.min(x_max_snapped);

                    (i, x_limited)
                })
                .min_by_key(|&(i, x_snapped)| {
                    // Compute squared distance to the original position.
                    let dx = (x_snapped - pos.x) as i64;
                    let dy = (row_y_coord[i] - pos.y) as i64;
                    dx * dx + dy * dy
                });

            if let Some((best_row, best_x)) = best_position {
                debug!("Best row: {}", best_row);
                // Found a position.
                let best_y = row_y_coord[best_row];

                // Update the row:
                debug_assert!(row_length[best_row] <= best_x);
                row_length[best_row] = best_x + w as db::SInt;

                // Make sure that cells face each other always nwell-to-nwell and pwell-to-pwell.
                // So cells in every second row must be rotated by 180 degrees (or alternatively mirrored).
                let flip = best_row % 2 == 1;
                let (rotation, x, y) = if flip {
                    (
                        db::Angle::R180,
                        best_x + w as db::SInt,
                        best_y + h as db::SInt,
                    )
                } else {
                    (db::Angle::R0, best_x, best_y)
                };
                let mirror = false;

                // Create transformation that puts the cell at the right location.
                let transformation = db::SimpleTransform::new(
                    mirror,
                    rotation,
                    1,
                    db::Vector::new(x + core_area.lower_left().x, y),
                );
                transformations.insert(cell_id, transformation);
            } else {
                panic!("No free row found.");
            }
        }

        transformations
    }
}
