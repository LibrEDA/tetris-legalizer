<!--
SPDX-FileCopyrightText: 2022 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# tetris-legalizer

Simple example detail-placer / legalizer for the LibrEDA framework.

The legalizer takes a rough global placement of standard-cells and finds a legal placement by snapping them to standard-cell
rows in an overlap-free way.

## Acknowledgements

* Since December 2020 this project is part of libreda.org and hence funded by [NLnet](https://nlnet.nl) and [NGI0](https://nlnet.nl/NGI0).

